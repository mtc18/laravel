<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase; //esto no es una clase
use App\User;

class UserTest extends TestCase{
    /**
     * A basic test example.
     *
     * @return void
     */

    //trait//cualidades de clases
    use RefreshDatabase; //hace que cada test se ejecute sobre una db limpia

    // public function test_lista_de_usuarios(){
    //     $response=$this->get('/users');
    //     $response->assertStatus(200);
    //     $response->assertSee('Lista de Usuarios');
    //     //$response->assertSee('Tony');
    //     $response->assertSee('Pepe');
    // }

    // public function test_show_de_usuarios(){
    //     $response=$this->get('/users/1');
    //     $response->assertStatus(200);
    //     $response->assertSee('Detalles de usuarios');
    // }

    public function test_lista_de_usuarios_vacia(){
      $response = $this->get('/users');
      $response->assertStatus(200);
      $response->assertSee('Lista de usuarios');
      $response->assertSee('No hay usuarios');
  }

  public function test_lista_de_usuarios(){
      factory(User::class)->create([
        'name' => 'Pepa',
        'email' => 'pepa@gmail.com'
    ]);

      $response = $this->get('/users');
      $response->assertStatus(200);
      $response->assertDontSee('No hay usuarios');
      $response->assertSee('Lista de usuarios');
      $response->assertSee('Pepa');
      $response->assertSee('pepa@gmail.com');
  }

  public function test_metodo_show_user_1(){
      factory(User::class)->create([
          'id' => 1,
          'name' => 'Pepa',
          'email' => 'pepa@gmail.com'
      ]);

      $response = $this->get('/users/1');
      $response->assertStatus(200);
      $response->assertSee('Detalles de usuarios');
      $response->assertSee('Pepa');
      $response->assertSee('pepa@gmail.com');
  }

  public function test_metodo_show_user_inexistente(){
    $response = $this->get('/users/100000');
    $response->assertStatus(404);
    }

}
