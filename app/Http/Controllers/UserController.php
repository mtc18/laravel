<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        //$user$users=array();
        // $users=array('Ramirin', 'Maite', 'Jorge', 'Tony', 'Antonio', 'Antoine', 'Anthony', 'Tonete', 'Gatete');
        $users=User::all();
        $users=User::paginate();

        return view('user.index', ['users' =>$users]);//para que te lleve a la vista correspondiente
        //busca el fichero /resourcers/views/user/index.php
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        //dd($request->all());//dd es un var_dump mas sofisticado
        //dd($request->name);//para ver atributos concretos

        $rules=[
            'name' => 'required|unique:users|max:255|min:5',
            'email' => 'required|unique:users|max:255|email',
            'password' => 'required|max:255',
            'color'=>'required|max:255',
        ];
        $request->validate($rules);


        $user=new User;
        // $user->name= $request->input('name');
        // $user->email= $request->input('email');
        // $user->password= bcrypt($request->input('password'));
        // $user->remember_token=str_random(10);//cadena aleatoria

        $user->fill($request->all());
        $user->save();

        return redirect('/users');

        //opcion2

        // $user = User::create($request->all());
        // return redirect('/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user){
        //return "Detalles de usuarios";
        // $user=User::find($id);//MANERA 1

        // if ($user==null) {
        //     abort(403, 'Prohibido');
        //     response()->view('errors404', [], 404);//MANERA 2
        // } DOS MANERAS DISTINTAS DE HACERLO

        //$user=User::findorFail($id);
        return view('user.show', [
            'user' => $user,
        ]);
        //dos maneras de hacer lo mismo

        // $otro = "Otra variablee";
        // return view('user.show', compact('id', 'otro'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $user = User::findOrFail($id);
        return view('user.edit',['user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){

        $rules=[
            'name' => 'required|unique:users|max:255|min:5',
            'email' => 'required|unique:users,email,$id,id|max:255|email',
        ];
        $request->validate($rules);


        $user=User::findorFail($id);
        $user->fill($request->all());
        $user->save();

        return redirect('/users/' . $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $user=User::findOrFail($id);
        $user->delete();
        return back();
    }

    public function especial(){
        $users=User::where('id', '>=', 5)
        ->where('id', '<=' , 10)
        ->get();

        dd($users);
        return "especial";
        return redirect('/users/' );
    }

}
