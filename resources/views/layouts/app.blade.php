<!DOCTYPE html>
<html>
<head>
    <title>@yield('content')</title>
</head>
<body>
    <div>
        @yield('content')
    </div>
</body>
</html>
