@extends('layouts.app')

@section('title','Lista de Usuarios')
@section('content')
    <h1>Lista de usuarios</h1>
    <a href="/users/create">Nuevo</a><hr>

    @forelse ($users as $user)
        <li>{{$user->name}}:{{$user->email}}:<a href="/users/{{$user->id}}/edit">Editar</a></li>

        <form method="post" action="/users/{{$user->id}}">
            {{csrf_field()}}
            <input type="hidden" name="_method" value="DELETE">
            <input type="submit" name="borrar" value="Borrar">
        </form>
    @empty
        <p>No hay usuarios</p>
    @endforelse
{{$users->render()}}

@endsection

