@extends('layouts.app')

@section('title','Usuarios')
@section('content')

<style type="text/css">

.alert{
    padding: 10px;
    background-color: #f88;
    margin:5px;
}
</style>


<h1>Alta de Usuarios</h1>

{{--
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
<hr>
--}}
@if ($errors->any())
<div class="alert alert-danger">
    HAY ERRORES
</div>
@endif
<hr>

<form method="post" action="/users">
    {{csrf_field()}} {{-- ESTO ES IMPORTANTE PORQUE SINO NO FUNCIONA, genera un input oculto con un valor aleatorio--}}
    <label>Nombre</label>
    <input type="text" name="name" value="{{old('name')}}"><br>
    <div>
        {{$errors->first('name')}}
    </div><br>

    <label>Email</label>
    <input type="text" name="email" value="{{old('email')}}"><br>
    <div>
        {{$errors->first('email')}}
    </div><br>

    <hr>

    <?php //TODO ESTO ES UNA ESPECIE DE CAPCHA PARA COMPROBAR QUE NO ERES BOT
    $example =['rojo', 'azul', 'verde']
    ?>
    <select name="color">
        @foreach ($example as $item)
        <option value="{{$item}}"
        {{old('color') ==$item ? 'selected="selected"' : '' }}>{{$item}}
    </option>
    @endforeach
</select>
{{old('color')}}
<br>

<label>Contraseña</label>
<input type="text" name="password"><br>
<div>
    {{$errors->first('password')}}
</div><br>

<input type="submit" value="Nuevo">

</form>
@endsection

