@extends('layouts.app')

@section('title','Usuarios')
@section('content')

<style type="text/css">

.alert{
    padding: 10px;
    background-color: #f88;
    margin:5px;
}
</style>

@if ($errors->any())
<div class="alert alert-danger">
    HAY ERRORES
</div>
@endif
<hr>

<h1>Edicion de Usuarios</h1>
<form method="post" action="/users/{{$user->id}}">
    {{csrf_field()}} {{-- ESTO ES IMPORTANTE PORQUE SINO NO FUNCIONA, genera un input oculto con un valor aleatorio--}}
    <input type="hidden" name="_method" value="PUT"><br>

    <label>Nombre</label>
    <input type="text" name="name"
    value="{{old('name') ? old('name') : $user->name}}">
    <div>
        {{$errors->first('name')}}
    </div><br>

    <label>Email</label>
    <input type="text" name="email"
    value="{{old('email') ? old('email') : $user->email}}">
    <br>
    <div>
        {{$errors->first('email')}}
    </div><br>

    <input type="submit" value="Guardar">

</form>
@endsection

