<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



// Route::get('users', function () {
//     return 'Lista de usuarios';
// });


// Route::get('users/{id}/{name?}', function ($id, $name=null) {
//     if ($name) {
//         return "Detalles del usuario $id. Su nombre es $name";
//     }
//     return "Detalles del usuario $id. Nombre Anonimo";
// });

// Route::get('users', 'UserController@index')->name('usuarios');
// Route::get('users/{$id}', 'UserController@show');
// Route::get('users/{id}', function ($id) {
//     return "Detalles del usuario $id";
// });
Route::get('users/especial', 'UserController@especial');
Route::resource("users", "UserController"); //ruta pa todo


//Route::get("users/especial, "UserController@especial"); //para metodos concretos, en este caso uno llamado especial
